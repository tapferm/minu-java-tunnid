package Day3;

public class LoopDemo {
    public static void main(String[] args) {
        //for tsükkel masiivi elementide välja printimiseks
        System.out.println("break keyword");
        String []linnad = {"6Tallinn", "6Helsinki", "6Madrid", "6Paris"};
        for(int i=0;i<linnad.length; i++){
            if(linnad[i].equals("6Madrid"))//see tingimus
                break;//kui üleval olev tingimus on täidetud siis break, elik lõpeta kõik.
            System.out.println("for tsükkel prindib linnad välja"+linnad[i]);
        }
        //for tsükkel kahe tasandilise masiivi jaoks
        String linnadRiikides[][]={
                {"Tallinn", "Tartu"},//1 rida - milles 4 veergu
                {"Stockholm", "Uppsala", "Lund", "Köping"},// 2 rida
                {"Helsinki", "Espoo", "Jämsä"}// 3 rida
        };
        for(int k=0;k<linnadRiikides.length; k++){//vertikaalne
            for(int j=0;j<linnadRiikides [k].length; j++)//horisontaalne

            System.out.println(linnadRiikides[k][j]);

        }
        //FOR EACH teeb sama mis tegi linnade  hulga printimiseks kirjutatud tsükkel, aga lihtsamini kirjutatud
        //rohkem automatiseeritud, peab ise väheam vaeva nägema
        String []linnad2 = {"Tallinn", "Helsinki", "Madrid", "Paris"};
        for(String myCity:linnad2){ //linnad2 - array mida lappan läbi; myCity ajutine muutuja kuju need salvestan
            System.out.println(myCity);
        }

        //kahe tasandiline masiiv for each

        String linnadRiikides2[][]={
                {"Pärnu", "Viljandi"},//1 rida - milles 4 veergu
                {"Puri", "Delhi", "Bubhaneswar", "Mumbai"},// 2 rida
                {"Pariis", "Marseille", "Nizza"}// 3 rida
        };
        for(String[] kohad:linnadRiikides2){//paremal masiiv, mida lappan
            for(String koht:kohad){//paremal masiiv mida lappan, nüüd lappan kohad masiivi mille lõin just üleval
                System.out.println(koht);
            }
        }

        //while(true);//lõputu tsükkel, teeb igavesti mitte midagi
        //WHILE TSÜKKEL siin pole nii palju väärtuseid ette antud pean need ise ehitama sisse
        //veel manuaalsem kui for tsükkel (for each kõige automaatsem)
        String []linnad3 = {"Tallinn5", "Helsinki5", "Madrid5", "Paris8"};
        int minuLinnadIndex=0;
        while (minuLinnadIndex<linnad3.length){
            System.out.println(linnad3[minuLinnadIndex]);
            minuLinnadIndex++;
        }
        //DO WHILE - käiakse tsükkel alati läbi vähemalt 1 kord. muidu on väga sarnane while-ile
        //nagu while aga ümberpööratud
        String []linnad4 = {"Valencia", "Bhubaneswar", "Berliin", "Addis Abbaba"};
        int uusIndex=0;
        do{ System.out.println(linnad4[uusIndex]);
            uusIndex++;
        }while(uusIndex<linnad4.length);

        //break kasutamine vaata üle fataalne sitatsioon







    }
}
