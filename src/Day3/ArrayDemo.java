package Day3;

public class ArrayDemo {
    public static void main(String[] args) {
        String []weekDays = {"Laupäev", "Pühapäev"};
        String [][] persons={ {"Mati","Kask","Võnnu"}, {"Malle", "Tamm","Kärdla"} };
        System.out.println("Viimane inimene elab kohas nimega " +persons[1][2]);//esimene viitab

        String [][] persons2 = new String[2][3]; //tegin uue masiivi ja määrasin selle suuruse
        persons2[0][0]="Mati"; //nii hakkan täitma üleval loodud massiivi
        persons2[0][1]="Kati";
        persons2[0][2]="Kalle";
        persons2[1][0]="Kask";
        persons2[1][1]="Tamm";
        persons2[1][2]="Jalakas";
        System.out.println(persons2[1][1]);

        //kahe kantsuluga kahe tasandiline massiv, kolmas tasand on ka mõeldav
        /*Ülesanne 7
Defineeri muutuja, mille tüübiks täisarvude massiiv.
Loo massiiv pikkusega 5 ja pane oma eelnevalt defineeritud muutuja sellele viitama.
Sisesta ükshaaval massiivi väärtused 1, 2, 3, 4, 5.
Prindi konsoolile esimese massiivielemendi väärtus.
Prindi konsoolile kolmanda massiivielemendi väärtus.
Prindi konsoolile viimase massiivielemendi väärtus.
*/
        int[]arvudeMassiiv= new int[5];
        arvudeMassiiv[0]=1;
        arvudeMassiiv[1]=2;
        arvudeMassiiv[2]=3;
        arvudeMassiiv[3]=4;
        arvudeMassiiv[4]=5;
        System.out.println(arvudeMassiiv[0]);
        System.out.println(arvudeMassiiv[2]);
        System.out.println(arvudeMassiiv[arvudeMassiiv.length-1]);
        /*Ülesanne 8
Defineeri muutuja, mille tüübiks tekstide massiiv.
Väärtusta see massiiv loomise hetkel väärtustega "Tallinn", "Helsinki", "Madrid", "Paris".
*/
        String[]tekstideMassiiv={"Tallinn", "Helsinki", "Madrid", "Paris"};

        /*Ülesanne 9
Defineeri kahetasandliline massiiv:
Element 1: 1, 2, 3
Element 2: 4, 5, 6
Element 3: 7, 8, 9, 0
*/
        int[][]kaheTasandilineMassiiv={{1, 2, 3},{4, 5, 6},{7, 8, 9, 0}};
        System.out.println(kaheTasandilineMassiiv[2][3]);
        System.out.println(kaheTasandilineMassiiv[2][kaheTasandilineMassiiv[2].length-1]);//sama mis üleval ,aga viimase elemendi võtan andmete jrgi


        //keerulisem tee, et seletada kuidas saab manipuleerida
        int[][]myCoolNUmbers= new int[3][]; //veergude arvu jätan lahtiseks
        myCoolNUmbers [0]=new int[3];//siin määran massivide pikkused masiivi sees
        myCoolNUmbers [1]=new int[3];
        myCoolNUmbers [2]=new int[4];
        //ja siin hakkan neile väärtuseid andma
        //rida 1
        myCoolNUmbers[0][0]=1;
        myCoolNUmbers[0][1]=2;
        myCoolNUmbers[0][2]=3;
        //rida 2
        myCoolNUmbers[1][0]=4;
        myCoolNUmbers[1][1]=5;
        myCoolNUmbers[1][2]=6;
        //rida 3
        myCoolNUmbers[2][0]=7;
        myCoolNUmbers[2][1]=8;
        myCoolNUmbers[2][2]=9;
        myCoolNUmbers[2][3]=0;



        /*Ülesanne 10
Defineeri kahetasandliline massiiv, milles hoida järgmisi linnu riikide kaupa:
Tallinn, Tartu, Valga, Võru
Stockholm, Uppsala, Lund, Köping
Helsinki, Espoo, Hanko, Jämsä
Väärtusta see massiiv kahel erineval viisil:
Element-haaval vt eelmist ülesannet kuidas teha element haaval
Inline-põhimõttel

*/
        //INLINE method
        String linnadRiikides[][]={
                {"Tallinn", "Tartu"},//1 rida - milles 4 veergu
                {"Stockholm", "Uppsala", "Lund", "Köping"},// 2 rida
                {"Helsinki", "Espoo", "Jämsä"}// 3 rida
        };

        System.out.println(linnadRiikides
                [linnadRiikides.length-1]//viimase rea =[2]
                [linnadRiikides[2].length-1]);//viimane veerg =[3] soomelinnade rida=linnadRiikides[2]
    }
}
