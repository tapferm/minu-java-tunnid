package Day3;

public class Harjutused {
    public static void main(String[] args) {
        //Ülesanne 11
        //Kirjuta while-tsükkel, mis prindiks konsoolile numbrid 1 ... 100 - iga number eraldi real.
        //WHILE TSÜKKEL siin pole nii palju väärtuseid ette antud pean need ise ehitama sisse
        int i=1;
        while (i<=100){
            //System.out.println(i);
            i++;
        }

        //Ülesanne 12
        //Kirjuta for-tsükkel, mis prindiks konsoolile numbrid 1 ... 100 - iga number eraldi real.
        for ( int j=1;j<=100 ;j++){
            //System.out.println(j);
        }
        /*Ülesanne 13 LAHENDAMATA
        Kirjuta foreach-tsükkel, mis prindiks konsoolile numbrid 1 ... 10 - iga number eraldi real.
        String []linnad2 = {"Tallinn", "Helsinki", "Madrid", "Paris"};
        */
       int oneToTen []={1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        for (int number:oneToTen) {
            //System.out.println(number);
        }
        /*Ülesanne 14
        Kirjuta for-tsükkel, mis prindiks konsoolile kõik arvud vahemikus 1 ... 100, mis jaguvad 3-ga - iga number eraldi real.
        if (Integer.parseInt(args[0]) % 2 == 0) {
        isEven = "The number is even";
        }*/
        int lugeja=0;

        for ( int j=1;j<=100 ;j++){
            if (j%3==0){
            lugeja++;
            System.out.println(j);
            }
        }
        System.out.println( lugeja +" numbrit vahemikus 1-100 jaguneb 3-ga");
        //teine variant astuda kolmese sammuga
        for (int h=3;h<=100; h+=3){
            System.out.println(h);
        }


        /*Ülesanne 15
        Defineeri massiiv väärtustega "Sun", "Metsatöll", "Queen", "Metallica".
        Prindi selle massiivi väärtused for-tsükli abiga konsoolile, eraldades nad komaga.
        Viimase elemendi järele koma panna ei tohi. Tulemus peaks olema: "Sun, Metsatöll, Queen, Metallica".
        */
        String[] bands={"Sun", "Metsatöll", "Queen", "Metallica", "Red Hot Chilli Peppers"};
        System.out.printf("\"");

        for(int b=0;b<bands.length;b++){
            System.out.printf(bands[b]);
            if (b==bands.length-1)
                break;
            System.out.printf(", ");
        }
        System.out.println("\"");

        // teine variant pane tsükkli sees kokku string mis prinditakse välja
        String lause=""; //loon string muutuja ja annan talle tühja väärtuse
        for (int a=0;a<bands.length;a++ ){
            lause= (lause +bands[a]);
            if(a==bands.length-1) //a väiksem kui bändide massiivi pikkus (see on bands. lenght -1
                break;
            lause= (lause + ", ");
        }
        System.out.println("\""+lause+"\"");

        // sisse ehitatud funktsiooniga
        String bandstext= String.join(", ", bands);
        System.out.println(bandstext);

        /*Ülesanne 16
Tee sama, mis eelmises ülesandes, ainult et prindi bändid välja tagantpoolt ettepoole.
*/
        System.out.printf("\"");
        for(int b=bands.length-1;b>=0;b--){
            System.out.printf(bands[b]);
            if (b==0)
                break;
            System.out.printf(", ");
        }
        System.out.println("\"");

        //teine variant kus kogus lause enne muutujasse kokku
        String lauseTagurpidi="";
        for (int m=bands.length-1;m>=0;m--){
            lauseTagurpidi=(lauseTagurpidi +bands[m]);
            if(m==0) // võib ka läheneda nii, et if m>0, siis lisatakse koma ja tühik
                break;
            lauseTagurpidi= (lauseTagurpidi + ", ");
        }
        System.out.println("\""+lauseTagurpidi+"\"");

        /*Ülesanne 17
Arenda Java programm, mis loeb käsurealt sisendina sisse numbrid vahemikus 0 - 9 ja prindib seejärel ekraanile sisestatud numbrid tekstilisel kujul.
Näiteks: sisendparameetrid 4 5 6, väljaprint: neli, viis, kuus.
*/
        String[] words={"null", "üks", "kaks", "kolm", "neli", "viis", "kuus", "seitse", "kaheksa", "üheks"};
        String jadaNumbreid="";//desineerin stringi kugu hakkan allpool teksti salvestama
        for(int p=0;p<args.length;p++){//kehtib kui p väiksem kui massiivi pikkus
            int arv=Integer.parseInt(args[p]);//teeb args[p] väärtuse arvuliseks väärtuseks
            jadaNumbreid=jadaNumbreid+words[arv];
            if (p<args.length){
                jadaNumbreid+=", ";
            }
        }
        System.out.println(jadaNumbreid);

        /*Ülesanne 18: do-while
Kirjuta do-while tsükkel, mis prindib välja "tere" vähemalt üks kord.
Defineeri double-tüüpi muutuja, milles hoida juhuslikku väärtust vahemikus 0 - 1.
Pärast igat "tere" väljatrükki konsoolile genereeri uus juhuslik number vahemikus 0 - 1 ja salvesta see eelpooldefineeritud muutujasse.
Jätka "tere" väljaprintimist niikaua, kui juhuslikult genereeritud numbri väärtus on väiksem, kui 0.5.
Vihje: Math.random()
*/
        int f=0;
        double juhuslik;//defineerin numbri tühja väärtusega
        do{ System.out.println("Tere");// see tingimus täidetakse ühe korra kindlsti, kas täidetakse veel, määran while osas
            juhuslik=Math.random();
            System.out.println(juhuslik);
        }while(juhuslik<=0.5);








}
}
