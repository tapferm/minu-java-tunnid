package Day2;

public class Day2exercises {
    public static void main(String[] args) {
        //float f=456.78F; panen F lõppu, muidu tahab defineerida double-ina
        //double myExtremelylargeDouble=3_432_569.65; võin kasutada alakriipse, et numbrit eraldada ja paremini lugeda
        double d = 456.78;
        String st = "test";
        boolean tode = d == 456.78;
        String s = "a";
        char c = 'a';
        int[] myIntegers = {5, 91, 304, 405};
        double[] myDoubles = {56.7, 45.8, 91.2};

        System.out.println(d);
        System.out.println(st);
        System.out.println(tode);
        System.out.println(s);
        System.out.println(c);
        System.out.println(myIntegers[3]);
        System.out.println(myDoubles[1]);
    }
}
