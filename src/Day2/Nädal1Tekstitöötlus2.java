package Day2;

import java.util.Scanner;

public class Nädal1Tekstitöötlus2 {
    public static final double VAT_RATE=1.3;//muutumatu muutuja defineerimine
    public static void main(String[] args) {
        String president1= "Konstantin Päts";
        String president2= "Lennart Meri";
        String president3= "Arnold Rüütel";
        String president4= "Toomas Hendrik Ilves";
        String president5= "Kersti Kaljulaid";

        StringBuilder presidendid= new StringBuilder().append(president1).append(", ").append(president2).append(", ").append(president3).append(", ").append(president4).append(", ").append(president5).append(" on eesti presidendid");
        System.out.println(presidendid.toString());

        String rida="\"Rida: See on esimene rida. Rida: See on teine rida. Rida: See on kolmas rida.\"";
        Scanner s= new Scanner("Rida: See on esimene rida. Rida: See on teine rida. Rida: See on kolmas rida.");//enamasti kasutatakse seda teksti sisse toomiseks väljaspoolt
        s.useDelimiter("Rida: ");//lõhu are seal kus näed seda mis sulgudes on
        System.out.println(s.next());
        System.out.println(s.next());
        System.out.println(s.next());
        final String myConstant="ccc";
        //myConstant="bbb";// seda muutujat ei saa ma muuta sest final panin ette

    }
}
