package Day2;

public class TingimusLaused {
    public static void main(String[] args) {
        int personAge=17;//kujuta ette et see tuleb väljast infona
        //täidetakse kas if või else blokk, üks kahest täidetakse alati
        if (personAge>=18) {
            System.out.println("Person is adult.");
            System.out.println("... ja midagi veel.");
        }else {
            System.out.println("Person is not adult.");
            System.out.println("ja midagi veel.");

            int temp=25;
            if(temp>=25) {
                System.out.println("on tavaline suveilm");
            }else
                if(temp>=20) { //if else lause, mitte else if blokk, oleneb kuidas taandada, aga sisulisest on alati if else ja järgmise sees if else
                    System.out.println("on tavaline suveilm");
                }else { //else blokki ei pea lõppuu panema, kui on isis on kindel et midagi täidetakse
                        System.out.println("Person is not adult.");
            }
        }
        //INLINE METHOD
        String isEven=Integer.parseInt(args[0])%2==0 ?"The number is even":"The number is odd"; //args[0] on muutuja mille kohta taham teada kas on paaris või paaritu
        System.out.println(isEven);

    }
}
