package Day2;

public class Nädal1Tekstitöötlus {
    public static void main(String[] args) {
        System.out.println("Hello, World!");
        System.out.println("Hello, \"World!\"");
        System.out.println("Steven Hawking once said: \"Life would be tragic if it weren't funny\".");
        String esimne = "See on teksti esimene pool  ";
        String teine = "See on teksti teine pool";
        System.out.println(esimne + teine);
        System.out.println("Elu on ilus.");
        System.out.println("Elu on 'ilus'.");//ühekordset jutumärki kahekordsete jutumärkide sees ei pea varjestama
        System.out.println("Elu on \"ilus\".");
        System.out.println("Kõige rohkem segadust tekitab \"-märgi kasutamine sõne sees.");
        System.out.println("Eesti keele kõige ilusam lause on: \"Sõida tasa üle silla!\"");
        System.out.println("'Kolm' - kolm, 'neli' - neli, \"viis\" - viis.");
        /*
        siia vahele võib kommentare kirjutada. kompilaator ignoreerib seda osa
         */
        String tallinnPopulation = "450 000";
        System.out.println("Tallinnas elab " + tallinnPopulation + " inimest");
        String text = String.format("Tallinns elab %s inimest", tallinnPopulation);
        System.out.println(text);
        System.out.printf("Tallinns elab %s inimest.\n", tallinnPopulation);//%s on sõne \n - viib järgmisele reale
        int populationOfTallinn = 450_000;
        System.out.printf("Tallinns elab %,d inimest.\n\n", populationOfTallinn);//%d on number %,d kummnendkohtade eraldaja, java ise määrab kuidas seda minu regionaal settingute järgi määratakse
        String bookTitle = "Rehepapp";
        System.out.println("Raamatu " + "\"" + bookTitle + "\"" + " autor on Andrus Kivirähk");
        String planeet1 = "Merkuur";
        String planeet2 = "Venus";
        String planeet3 = "Maa";
        String planeet4 = "Marss";
        String planeet5 = "Jupiter";
        String planeet6 = "Saturn";
        String planeet7 = "Uran";
        String planeet8 = "Neptun";
        int planetCount = 8;
        String text3 = String.format("%s, %s, %s, %s, %s, %s, %s, %s, on Päikesesüsteemi %s planeeti.", planeet1, planeet2, planeet3, planeet4, planeet5, planeet6, planeet7, planeet8, planetCount);
        System.out.println(text3);
        //StringBuilder
        //StringBuffer
        StringBuilder sb= new StringBuilder();
        sb.append("See on teksti jupp 1 ");
        sb.append("See on teksti jupp 2 ");
        sb.append("See on teksti jupp 3 ");
        sb.append("See on teksti jupp 4 ");
        sb.append(445577);
        System.out.println(sb.toString());


        String names= "Mari, Malle, Kalle, Juku";
        String [] namesArray=names.split(",");
        System.out.println(namesArray[2]);

        String city="New York";
        System.out.println("Kui pikk sa oled? " +city.length()); //loeb tähed ja tühikud
        System.out.println("esimene täht on: " + city.charAt(0));
        System.out.println("viimane täht on: " + city.charAt(city.length()-1)); //viimane täht on kogu sõne pikkus-1 ja saan viimase tähe asukoha
        System.out.println("+".repeat(25)); //kordab minu stringi nii mitu korda kui sulgudesse kirjutan



    }
}
