package Day2;

public class Day2String {
    public static void main(String[] args) {
        String s1 = args[0];
        String s2 = args[1];
        boolean areParamatresEqual = s1 == s2;
        System.out.println(areParamatresEqual);
        s2 = s1;
        areParamatresEqual = s1 == s2;
        System.out.println(areParamatresEqual);
        String s3 = args[0];
        String s4 = args[1];
        System.out.println(s3.equals(s4));
        System.out.println(s3.equalsIgnoreCase(s4));
        String myGeeting1 = "Hello";
        String myGeeting2 = "Hello";
        System.out.println("kas on võrdsed " + (myGeeting1 = myGeeting2));
        // siin on võrdsed sest java siseselt antud parameetrid loeb võrdseks-optimeerib. väljaspoolt minu programmi tulevat infot ei oska nii võrrelda.
        //String quoute ="Isa ütles "Too puid""; seda ei loe sest teised jutumärgi on arusaamatud
        String quoute = "Isa ütles \"Too puid\""; //\ see varjestab jutumärgid
        String multilinetext = "rida1 \n\trida2\n\trida3";
        System.out.println(quoute);
        System.out.println(multilinetext);
    }

}
