package Day2;

public class Koodikonstruktsioonid {
    public static void main(String[] args) {
        int vanus=100;
        String ageDesc =(vanus>100)? "Vana":"Noor";
        System.out.println(ageDesc);

        String ageDesc2 =(vanus>100)? "Vana":((vanus==100) ? "Peaaegu vana":"Noor");//siin panin inlineif struktuuri sisse teise inlineif-i
        System.out.println(ageDesc2);
        String linn="Berlin";
        if(linn.equals("Milano")){
            System.out.println("Ilm on soe.");
        }else{
            System.out.println("Ilm polegi kõige tähtsam!");
        }

        //String isEven=Integer.parseInt(args[0])%2==0 ?"The number is even":"The number is odd"; //args[0] on muutuja mille kohta taham teada kas on paaris või paaritu

        int grade = Integer.parseInt(args [2]);
        if(grade==1){
            System.out.println("nõrk");
        }
        else if(grade==2){
            System.out.println("mitterahuldav");
        }
        else if(grade==3){
            System.out.println("rahuldav");
        }
        else if(grade==4){
            System.out.println("hea");
        }
        else if(grade==5){
            System.out.println("väga hea");
        }

        //switch selle asemel ei saa võrrelda vahemikke, vaid kindlaid (diskreetseid väärtused) väärtuseid
        switch (grade){
            case 1:
                System.out.println("nõrk");
                break; // see tagab, et kui see case kehtib lõpetatake programm
            case 2:
                System.out.println("mitterahuldav");
                break;
            case 3:
                System.out.println("rahuldav");
                break;
            case 4:
                System.out.println("hea");
                break;
            case 5:
                System.out.println("väga hea");
                break;
            default:
                System.out.println("sisestasid vale väärtuse");
                break;


        }
        switch (grade) {
            case 5:
            case 4:
            case 3:
                System.out.println("PASS"); // saan erinevad cas-id kokku koondada
                break;
            default:
                System.out.println("FAIL");
                break;
        }
    }

}
