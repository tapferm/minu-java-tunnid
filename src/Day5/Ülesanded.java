package Day5;

import java.util.*;

public class Ülesanded {
    public static void main(String[] args) {
        //Ülesanne 1
        for (int i = 6; i > 0; i--) {
            System.out.println("#".repeat(i));
        }
        //variant 2
        for( int i=1; i<=6; i++){
            int colCount=7-i;
            for(int j= 0; j<colCount; j++) {
                System.out.print("#");
            }
            System.out.println();
        }
//Ülesanne 2
        for (int i = 1; i <= 6; i++) {
            System.out.println("#".repeat(i));
        }
        //Ülesanne 3
        int j = 0;
        for (int i = 5; i > 0; i--) {
            j++;
            System.out.println(" ".repeat(i) + "@".repeat(j));
        }
        //teine variant kahe for tsükkliga
        //Ülesanne 4

        int lähtenumber = 1234567890;
        String number = ((Integer) lähtenumber).toString();
        char[] numbrid = number.toCharArray();//numbrid ==> char[7] { '1', '2', '3', '4', '5', '6', '7' }

        for (int i = numbrid.length; i > 0; i--) {
            System.out.print(numbrid[i - 1]);
        }
        System.out.println();
        //proovi ka for each tsükkliga

        //variant 2  kasutan sama lähtenumbrit siin
        StringBuilder str = new StringBuilder(number);
        StringBuilder reverseStr= str.reverse();//pöörab Stringi ümber
        System.out.println(reverseStr);
        //Ülesanne 5
        //
        //Kirjuta main-meetod, mis loeb käsurealt õpilase nime ja tema eksami punktide summa.
        // Kui summa on väiksem kui 51 punkti,
        // väljastatakse sõna “[NIMI]: FAIL”,
        // muudel juhtudel väljastatakse “[NIMI]: PASS - [HINNE], [PUNKTI SUMMA]”,
        // kus hinne on arvutatud järgmiselt:
        //1 = 51-60
        //2 = 61-70
        //3 = 71-80
        //4 = 81-90
        //5 = 91-100
        int punktid= Integer.parseInt(args[1]);
        int hinne=0;
        String nimi= args[0];
        if(punktid<51){
            System.out.println(args [0]+": FAIL");
        }else if(punktid>=51&&punktid<=60){
            hinne=1;
            System.out.println(args [0]+" : PASS - " + hinne+", "+ punktid);
        }else if(punktid>=61&&punktid<=70){
            hinne=2;
            System.out.println(args [0]+" : PASS - " + hinne+", "+ punktid);
        }else if(punktid>=71&&punktid<=80) {
            hinne =3;
            System.out.println(args [0]+" : PASS - " + hinne+", "+ punktid);
        }else if(punktid>=81&&punktid<=90) {
            hinne = 4;
            System.out.println(args [0]+" : PASS - " + hinne+", "+ punktid);
        }else if(punktid>=91&&punktid<=100) {
            hinne = 5;
            System.out.println(args [0]+" : PASS - " + hinne+", "+ punktid);
        }

        // variant 2
        if (punktid>90){
            hinne=5;
        }else if (punktid>80){
            hinne=4;
        }else if (punktid>70){
            hinne=3;
        }else if (punktid>60){
            hinne=2;
        }else if (punktid>50){
            hinne=1;
        }
        if (hinne>0){
            System.out.printf("%s: Pass - %d, %d\n", nimi, hinne, punktid);
        }else {
            System.out.printf("%s: FAIL\n", nimi);
        }
        //variant 3
        for(int i=0;i<args.length;i+=2){
            nimi=args[i];
            punktid = Integer.parseInt(args[i+1]);

        }
        /*
        Ülesanne 6

Loo komaga arvudega massiiv (kahetasandiline massiiv), kus oleks kaks arvu igas reas.
Massiivi läbides trüki välja nende arvude ruutude summa ruutjuur.
VASTUS =a2+b2.
Vaja on kasutada matemaatilisi funktsioone Math.pow() ja Math.sqrt().

*/
        double[][]kahetasandilineMassiiv={
                {6.7, 4.8},
                {3.5, 6.7},
                {4.0, 3.0}};

        for(double [] reaKaupa:kahetasandilineMassiiv){
            double a=reaKaupa[0];
            double b= reaKaupa[1];
            double c= Math.sqrt((Math.pow(a,2)+Math.pow(b,2))); //Math.pow(5,2) on viis astmel 2 elik 5 ruudus
            System.out.printf("Kui külg a=%.2f ja külg b=%.2f, siis külg c= %.2f\n", a,b,c);
        }
        /*

Ülesanne 7

Tekita kahetasandiline massiiv, mis hoiaks infot riigi, selle pealinna, ja peaministri nimega (massiivi minimaalsed mõõtmed: 10X3) Näiteks:
Estonia, Tallinn, Jüri Ratas
Latvia, Riga, Arturs Krišjānis Kariņš
...
Trüki välja ainult riikide peaministrid - iga nimi eraldi real.
Trüki välja iga riigi kohta rida: Country: XXX, Capital: YYY, Prime minister: ZZZ

*/
        String [][]riigiInfo={
                {"Estonia", "Tallinn", "Jüri Ratas"},
                {"France", "Paris", "Édouard Philippe"},
                {"Spain", "Madrid", "Pedro Sánchez"},
                {"Ethiopia", "Addis Ababa","Abiy Ahmed"},
                {"India", "Delhi", "Narendra Modi"},
                {"Hungary","Budapest", "Viktor Orbán"},
                {"Italy", "Rome", "Giuseppe Conte"},
                {"Sweden", "Stocholm", "Stefan Löfven"},
                {"Latvia", "Riga", "Arturs Krišjānis Kariņš"},
                {"Finland","Helsinki", "Sanna Marin"}};
        System.out.println("Peaministrid:");
        for(String[]riigiKaupa:riigiInfo){
            System.out.println(riigiKaupa[2]);
        }
        System.out.println("Riikide info: ");
        for(String[]riigiKaupa:riigiInfo){
            System.out.printf("Country: %s, Capital: %s, Prime minister: %s\n",riigiKaupa[0],riigiKaupa[1],riigiKaupa[2]);
        }
        /*
Ülesanne 8

Tekita eelmise ülesandega sarnane massiiv, aga lisa igale riigile veel ka selles riigis räägitavad levinumad keeled. Näiteks:
Estonia, Tallinn, Jüri Ratas, [Estonian, Russian, Ukrainian, Belarusian, Finnish]
Latvia, Riga, Māris Kučinskis, [Latvian, Russian, Belarusian, Ukrainian, Polish]
...

Prindi välja riigi nimi ja selle alla kõik räägitavad keeled. Umbes nii:
Estonia:
Estonian
	Russian
	Ukrainian
	Belarusian
	Finnish

	*/
        String [][][]riigiInfo2={
                {{"Estonia"}, {"Tallinn"}, {"Jüri Ratas"}, {"Estonian", "Russian","Ukrainian"}},
                {{"France"}, {"Paris"}, {"Édouard Philippe"}, {"French","Arabic"}},
                //{"Spain", "Madrid", "Pedro Sánchez"},
                {{"Ethiopia"}, {"Addis Ababa"},{"Abiy Ahmed"},{"Oromo", "Amharic"}},
                //{"India", "Delhi", "Narendra Modi"},
                //{"Hungary","Budapest", "Viktor Orbán"},
                //{"Italy", "Rome", "Giuseppe Conte"},
                //{"Sweden", "Stocholm", "Stefan Löfven"},
                //{"Latvia", "Riga", "Arturs Krišjānis Kariņš"},
                //{"Finland","Helsinki", "Sanna Marin"}
        };
        System.out.println("Languages: ");
        for(String[][]riigiKaupa2:riigiInfo2) { //kahetasandiline massiiv
            System.out.println(riigiKaupa2[0][0]+":");
            for (String andmeKaupa : riigiKaupa2[3]) { //lappan läbi keeled
                System.out.println(andmeKaupa);
            }
        }

        //variant 2 for tsükkliga
       /*for (int i=0;i<riigiInfo2.length;i++){
           System.out.println(riigiInfo2[i][0][0]+" "+riigiInfo2[i][2][0]);
           for(int k=0;k< riigiInfo2[i][3].length;k++) {
               System.out.println(riigiInfo2[i][3][k]);
           }
       }*/

        /*

Ülesanne 9

Korda eelnevat ülesannet, aga kasuta andmete hoidmiseks List-tüüpi objekti.

*/
        //Arrays.aslist
        List<List<List<String>>> riigid6 = //listide listide list -- siin on mingi viga
                Arrays.asList(
                        Arrays.asList(
                                Arrays.asList("Estonia"),
                                Arrays.asList("Tallinn"),
                                Arrays.asList("Jüri Ratas"),
                                Arrays.asList("Estonian", "Russian", "Ukrainian"),

                                Arrays.asList("France"),
                                Arrays.asList("France"),
                                Arrays.asList("Édouard Philippe"),
                                Arrays.asList("French", "Arabic"),

                                Arrays.asList("Ethiopia"),
                                Arrays.asList("Addis Ababa"),
                                Arrays.asList("Abiy Ahmed"),
                                Arrays.asList("Oromo2", "Amharic2")
                        )
                );
        for(int i=0;i<riigid6.size();i++){ // array listis on minig viga size on 1
            System.out.println(riigid6.get(i).get(0).get(0));
        }
        // for each
        /*
Ülesanne 10

Korda eelnevat ülesannet, aga kasuta andmete hoidmiseks Map-tüüpi objekti, kus key-väärtus oleks riigi nimi.
        Map<String,String> estonianEnglishDic = new HashMap<>();//võti on String, ja väärtus on String
        estonianEnglishDic.put("auto", "car");// siin annan väärtuse .put-iga
        estonianEnglishDic.put("puu", "tree");
        estonianEnglishDic.put("maja", "house");
        estonianEnglishDic.put("võti", "key");
        System.out.println(estonianEnglishDic);//oskab ennast välja printida
        System.out.println(estonianEnglishDic.get("maja"));//key-le vastav sõna

        //küsi countriesMap-i keyset ja pane see massiivi
        String [] keys= countriesMap.keySet().toArray(String[]::new);
        // võta see massiiv keys ja prindi need riigid ühe kaupa loop-iga välja
        for (int i=0; i<keys.length; i++){// sellega lähen võtmete kaupa sisse
            String myCurrentCountryName = keys[i]; //deklareerim muutuja minu hetkel käsil oleva riigi nime kohta
            System.out.println("Country: " + myCurrentCountryName);
*/
        Map<String,String[][]> riigidMap=new HashMap<>();// key on string ja value on kahe tasandiline massiiv

        riigidMap.put("Estonia.",
                new String[][]{{"Tallinn"}, {"Jüri Ratas"}, {"Estonian", "Russian","Ukrainian"}}// annan väärtuse key-le ja kahetasandilise massiivile
        );
        riigidMap.put("France.",
                new String[][]{{"Paris"}, {"Édouard Philippe"}, {"French","Arabic"}}
        );
        riigidMap.put("Ethiopia.",
                new String[][]{{"Addis Ababa"},{"Abiy Ahmed"},{"Oromo", "Amharic"}}
        );
        for (String riigid7:riigidMap.keySet()){
            System.out.println(riigid7+", "+riigidMap.get(riigid7)[1][0]);
            for (String keeled:riigidMap.get(riigid7)[2]) {//lappab läbi riigidMap(riigid7) indexiga 2 st kolmas masiiv
                System.out.println("\t" + keeled);

            }
        }


        /*



Ülesanne 11

Korda eelnevat ülesannet, aga kasuta andmete hoidmiseks Queue-tüüpi objekti.
*/
        Queue<String[][]> riigiQue = new LinkedList<>();//teen Queue tüüpi kollektsiooni kahe tasandilisest massiivist
        riigiQue.add(new String[][]{{"Estonia"}, {"Tallinn"}, {"Jüri Ratas"}, {"Estonian", "Russian","Ukrainian"}});
        riigiQue.add(new String[][]{{"France"}, {"Paris"}, {"Édouard Philippe"}, {"French","Arabic"}});
        riigiQue.add(new String[][]{{"Ethiopia"}, {"Addis Ababa"},{"Abiy Ahmed"},{"Oromo", "Amharic"}});

        while (!riigiQue.isEmpty()){
            String [][]riik= riigiQue.remove();
            System.out.println(riik[0][0]+" "+riik[2][0]);
            for (String language:riik[3]){
                System.out.println("\t"+language);
            }
        }
    }
}
