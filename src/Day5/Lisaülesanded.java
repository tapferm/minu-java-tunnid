package Day5;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Lisaülesanded {
    public static void main(String[] args) {
/*Ülesanne 2: Stringitöötlus, for-tsükkel, kollektsioonid & massiivid
Defineeri String-tüüpi muutuja ja väärtusta see järgmise tekstiga:
Loe sellest muutujast välja kasutajaandmed ja sisesta need:
a) Stringide massiivi
b) Listi
c) Mappi

Prindi iga muutujatüübi puhul tulemus ekraanile loetavas formaadis.

*/

String text="Eesnimi: Teet, Perenimi: Kask, Vanus: 34, Amet: lendur, Kodakondsus: Eesti; Eesnimi: Mari, Perenimi: Tamm, Vanus: 56, Amet: kosmonaut, Kodakondsus: Soome; Eesnimi: Kalle, Perenimi: Kuul, Vanus: 38, Amet: arhitekt, Kodakondsus: Läti; Eesnimi: James, Perenimi: Cameron, Vanus: 56, Amet: riigiametnik, Kodakondsus: UK; Eesnimi: Donald, Perenimi: Trump, Vanus: 73, Amet: kinnisvaraarendaja, Kodakondsus: USA";
String []persons=text.split("; ");
//String[5] {
// "Eesnimi: Teet, Perenimi: Kask, Vanus: 34, Amet: lendur, Kodakondsus: Eesti",
// "Eesnimi: Mari, Perenimi: Tamm, Vanus: 56, Amet: kosmonaut, Kodakondsus: Soome",
// "Eesnimi: Kalle, Perenimi: Kuul, Vanus: 38, Amet: arhitekt, Kodakondsus: Läti",
// "Eesnimi: James, Perenimi: Cameron, Vanus: 56, Amet: riigiametnik, Kodakondsus: UK",
// "Eesnimi: Donald, Perenimi: Trump, Vanus: 73, Amet: kinnisvaraarendaja, Kodakondsus: USA" }
       // String[] elements=persons.split(", ")[0];
        String[][] person2= new String[persons.length][5];
        int index=0;

        for (String elements:persons){
            String []details=elements.split(", "); //poolitan ", " kohalt
            for (int i=0;i<details.length;i++){
                String [] halfCleaned =details[i].split(": ");//jagan kirjleduse(nimi:) ja detailid(Teet) ": " pooleks
                details [i]=halfCleaned[1]; // ja võtan teise osa , mis on detail (teet) ja panen selle detailide arraysse
            }
            person2[index]=details; //panen info ühe inimese kohta elementideks jagatuna arrayssse
            index++;
        }
        //pane need andmed mapi MIDAGI EI TÖÖTA MAP ON TÜHI

        Map<String, List<String>> personsMap =new HashMap<>();//map, esimene element string, teine element list stringidest

        for (String personDetailsTxt:text.split("; ")) {
            // "Eesnimi: Teet, Perenimi: Kask, Vanus: 34, Amet: lendur, Kodakondsus: Eesti",
            String []personDetails= personDetailsTxt.split(", ");
            // "Eesnimi: Teet", "Perenimi: Kask", "Vanus: 34", "Amet: lendur", "Kodakondsus: Eesti",
            String personKey=personDetails[0].split(": ")[1]+" "+personDetails[1].split(": ")[1];
            // personKey="Teet Kask"
            List<String> personValue= Arrays.asList(
                    personDetails[2].split(": ")[1],
                    personDetails[3].split(": ")[1],
                    personDetails[4].split(": ")[1]
            );
            //personValue={34, lendur, Eesti}
            personsMap.put(personKey,personValue);
        }
        System.out.println(personsMap);
        // Ülesanne
        //#+#+#+#+#+#+#+#+#+#
        //+#+#+#+#+#+#+#+#+#+
        //#+#+#+#+#+#+#+#+#+#
        //+#+#+#+#+#+#+#+#+#+
        //#+#+#+#+#+#+#+#+#+#
        //+#+#+#+#+#+#+#+#+#+
        //#+#+#+#+#+#+#+#+#+#
        //+#+#+#+#+#+#+#+#+#+
        for (int k = 1; k <= 8; k++) {//kõrgus
            for(int l=1; l<=19;l++){
                if (k%2 ==1) {
                    System.out.print(l%2 ==1? "#":"+"); //inline if määrab kumma sümboli prindime
                }else {
                    System.out.print(l%2 ==1? "+":"#");
                }
            }
            System.out.println();

        }
        /*

#
-#
--#
---#
----#
-----#
*/
        int ridadeArv=6;
        for(int i=0;i<ridadeArv;i++){
            for(int j=0;j<i;j++){ //mitu korda see tsükkel läbitakse määrab rea pikkuse
            System.out.print("-");
            }
            System.out.println("#");//siin toimub rea vahetus
        }
        /*
#++#++#
=======
#++#++#
=======
#++#++#
(" ".repeat(i) + "#")
System.out.print(l%2 ==1? "#":"+")
        */
        for(int h=0;h<5;h++) {
            for (int i = 0; i < 3; i++) {
                System.out.print(i < 2 ? "#++" : "#");
            }
            System.out.println();
            System.out.println("=======");

        }




    }
}
