package Weekend;

import java.util.*;

public class IgavusHarjutused {
    public static void main(String[] args) {
        /*
        1) Kasutades for-tsükleid ja if-lauseid, kuva ekraanil järgnev muster...
        */
        for (int i = 0; i < 9; i++) {//kõrgus
            //siia jahunemine paaritu a variant paaris b
            if (i % 2 == 0) {
                for (int j = 0; j < 3; j++) {//laius
                    System.out.print("###+++");
                }
            } else {
                for (int j = 0; j < 3; j++) {//laius
                    System.out.print("+++###");
                }
                //System.out.print("+++###");
            }
            System.out.println();//läheb järgmisele reale
        }
        //2
        for (int i = 0; i < 3; i++) {//kõrgus
            //siia jagunemine paaritu a variant paaris b

            for (int h = 0; h < 3; h++) {
                for (int j = 0; j < 10; j++) {//laius
                    System.out.print("#+");
                }
                System.out.println();//läheb järgmisele reale
            }
            for (int h = 0; h < 3; h++) {
                for (int j = 0; j < 10; j++) {//laius
                    System.out.print("+#");
                }
                System.out.println();//läheb järgmisele reale
            }
        }
        //3
        String lause = "#";
        System.out.println(lause);
        System.out.println(lause);
        for (int i = 0; i < 18; i++) {//kõrgus
            lause = " " + lause;
            System.out.println(lause);
        }
//4
        for (int i = 18; i > 0; i--) {
            System.out.println(" ".repeat(i) + "#");
        }

    }
}
