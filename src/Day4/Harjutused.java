package Day4;

import javax.swing.*;
import java.util.*;

public class Harjutused {
    public static void main(String[] args) {
        /*Ülesanne 19: List
Defineeri List-tüüpi kollektsioon (ArrayList), mis hoiaks endas String-tüüpi elemente.
Sisesta kollektsooni viis suvalist linnanime.
Prindi konsoolile esimene, kolmas ja viimane element sellest kollektsioonist.
*/

        List<String> linnad = new ArrayList<>();
        linnad.add("Pärnu");
        linnad.add("Viljandi");
        linnad.add("Paide");
        linnad.add("Kohila");
        linnad.add("Narva");
        System.out.println(linnad.get(0)+" "+linnad.get(2)+" "+linnad.get(linnad.size()-1));
        /*Ülesanne 20: Queue
Defineeri Queue-tüüpi kollektsioon (LinkedList), mis hoiaks endas String-tüüpi elemente.
Sisesta kollektsiooni kuus suvalist nime.
Kasutades while-konstruktsiooni, prindi kõik nimed ükshaaval konsoolile.

/while(true);//lõputu tsükkel, teeb igavesti mitte midagi
        //WHILE TSÜKKEL siin pole nii palju väärtuseid ette antud pean need ise ehitama sisse
        //veel manuaalsem kui for tsükkel (for each kõige automaatsem)
        String []linnad3 = {"Tallinn5", "Helsinki5", "Madrid5", "Paris8"};
        int minuLinnadIndex=0;
        while (minuLinnadIndex<linnad3.length){
            System.out.println(linnad3[minuLinnadIndex]);
            minuLinnadIndex++;
        }
*/
        Queue<String> riigid = new LinkedList<>();
        riigid.add("Eesti");
        riigid.add("Saksamaa");
        riigid.add("Norra");
        riigid.add("Suurbritannia");
        riigid.add("Hispaania");
        //riigid.poll(); //get ei tööta selle puhul
        System.out.println(riigid);
        System.out.println(riigid.isEmpty());//tagastab boolena väärtuse
        //System.out.println(riigid.peek());
        String print="";
        while (!riigid.isEmpty()){
            System.out.println(riigid.poll());
        };

        Map<String,String[]> countriesMap= new HashMap<>();//võti string ja väärtus stringide massiv
        countriesMap.put("Estonia", new String[]{"Tallinn", "Tartu", "Pärnu"});
        countriesMap.put("Soome", new String[]{"Helsinki", "Espoo", "Hanko"});
        countriesMap.put("Prantsusmaa", new String[]{"Pariis", "Marseille", "Nizza"});

        /*//küsi countriesMap-i keyset ja pane see massiivi
        String [] keys= countriesMap.keySet().toArray(String[]::new);
        // võta see massiiv keys ja prindi need riigid ühe kaupa loop-iga välja
        for (int i=0; i<keys.length; i++){// sellega lähen võtmete kaupa sisse
            String myCurrentCountryName = keys[i]; //deklareerim muutuja minu hetkel käsil oleva riigi nime kohta
            System.out.println("Country: " + myCurrentCountryName);

            System.out.println("Cities: ");
            String[] myCurrentCitys =countriesMap.get(myCurrentCountryName);//tegin võtmele vastavast väärtusest massiivi
            // võtmele vastava väärtuse, mis on massiiv saan välja printida for tsükkliga
            for(int j=0; j<myCurrentCitys.length;j++){
                System.out.println("\t"+myCurrentCitys[j]); //  "\t" paneb taande

                String []linnad2 = {"Tallinn", "Helsinki", "Madrid", "Paris"};
        for(String myCity:linnad2){ //linnad2 - array mida lappan läbi; myCity ajutine muutuja kuju need salvestan
            System.out.println(myCity);
        }*/
                //lühem variant for each tsükkliga - proovi ise teha
        String [] keys= countriesMap.keySet().toArray(String[]::new);
        for(String myCountryName:keys){
            System.out.println("Country: "+ myCountryName);//see prindib välja riikide nimed
            System.out.println("Cities: ");
            String[] myCurrentCitys =countriesMap.get(myCountryName);
            for(String citys:myCurrentCitys){
                System.out.println("\t"+citys);

            }

        }

    }
}
