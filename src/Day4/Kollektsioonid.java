package Day4;

import java.util.*;

public class Kollektsioonid {
    public static void main(String[] args) {
        //massiv täisarvudest
        int[] myArray={1,3,8,9,8};
        myArray[2]=89;//saan selle asendada, aga kuuendat elementi lisada ei saa
        System.out.println(myArray[2]);

        //list täisarvudest
        List<Integer> myList = new ArrayList<>();// ei saa kasutada primitiivset muutujat
        myList.add(1);//elementide lisamine
        myList.add(3);
        myList.add(8);
        myList.add(9);
        myList.add(8);
        System.out.println(myList); //oskab ennast välja printida
        System.out.println("Listi kolmas element on " +myList.get(2)); //listi elemendile ligi pääsemine
        myList.remove(1);// kustutab kinda indeksiga objekti
        System.out.println(myList);
        myList.remove((Integer)1);// kustutab selle elemendi mis määran, olenemata selle asukohast, aga kustutab esimese mis leiab
        System.out.println(myList);
        System.out.println("Kas sa sisaldad elementi 8? "+myList.contains(8)); // küsin kas sisaldab kaheksat
        System.out.println("Elemendi 8 index on "+ myList.indexOf(8));//küsin kus asub number kaheks listis
        System.out.println("Elemendi 2 index on "+ myList.indexOf(2));//küsin kus asub number kaks listis - seda pole vstus -1
        System.out.println("Listi pikkus on "+myList.size());//listi pikkus
        myList.set(0,5);//asendmaine esimesena määran indexiga asukoha, ja teisena määran mis sinna pannakse
        System.out.println(myList);
        myList.add(1,3);//index määrab asukoha ja teised liiguvad edasi ühe koha võrra
        System.out.println(myList);

        //Set,Hashset, Treeset // setist ei saa indexiga elemente kätte
        Set<String> names = new HashSet<>();
        names.add("Malle");
        names.add("Mai");
        names.add("Meelis");//ühte asja mitu korda ei panda
        names.add("Meelis");
        names.add("Meelis");
        names.add("Meelis");
        names.add("Meelis");
        names.add("Mihkel");
        names.add("Toomas");
        names.add("Rainer");
        names.add("Ahti");
        System.out.println(names);// [Ahti, Mai, Rainer, Meelis, Malle, Mihkel, Toomas]

// Kui tahan kasutada tavalist for tsüklit, pean seti konverteerima massiviks
        String[] namesArray=names.toArray(String[]::new);
        for(int i=0;i<namesArray.length;i++) {
            System.out.println(namesArray[i]);
        }
        //java 8 uus feature
        names.forEach(midaIganes -> System.out.println(midaIganes));//sulgudes lambda avaldis. võta sisend parameeter name ja rakenda talle seda funktsiooni. minaIganes muutujatat ei defineeri kusagil mujal ja tüübi tunneb see avaldis ise ära
/*Ülesanne 21: Set
Defineeri Set-tüüpi kollektsioon (TreeSet), mis hoiaks String-tüüpi elemente.
Prindi kollektsiooni sisu konsoolile, kasutades selle kollektsiooni forEach()-meetodit.
*/
        Set<String> puud = new TreeSet<>();
        puud.add("Paju");
        puud.add("Tamm");
        puud.add("Jalakas");
        puud.add("Lepp");
        puud.add("Kuusk");

        //variant 1
        String[] puudMassiiv=puud.toArray(String[]::new);
        for(int i=0;i<puudMassiiv.length;i++){
            System.out.println(puudMassiiv[i]);
        }
        //variant 2
        puud.forEach(x-> System.out.println(x));

        //MAP HASHMAP

        Map<String,String> estonianEnglishDic = new HashMap<>();//võti on String, ja väärtus on String
        estonianEnglishDic.put("auto", "car");// siin annan väärtuse .put-iga
        estonianEnglishDic.put("puu", "tree");
        estonianEnglishDic.put("maja", "house");
        estonianEnglishDic.put("võti", "key");
        System.out.println(estonianEnglishDic);//oskab ennast välja printida
        System.out.println(estonianEnglishDic.get("maja"));//key-le vastav sõna
        System.out.println(estonianEnglishDic.keySet());//prindib kõik key-d
/*Ülesanne 22: Map
Defineeri Map-tüüpi objekt (HashMap), mille key-element oleks String tüüpi, ja value-element oleks Stringide massiiv.
Sisesta sellesse objekti järgmine struktuur:
Estonia -> Tallinn, Tartu, Valga, Võru
Sweden -> Stockholm, Uppsala, Lund, Köping
Finland -> Helsinki, Espoo, Hanko, Jämsä
Prindi see struktuur välja, kasutades for-tsükleid (kaks tükki).
Prindi see struktuur välja, kasutades map-objekti forEach()-meetodit.

String []linnad2 = {"Tallinn", "Helsinki", "Madrid", "Paris"};
        for(String myCity:linnad2){ //linnad2 - array mida lappan läbi; myCity ajutine muutuja kuju need salvestan
            System.out.println(myCity);
        }
*/
        Map<String,String[]>countriesMap= new HashMap<>();//võti string ja väärtus stringide massiv
        countriesMap.put("Estonia", new String[]{"Tallinn", "Tartu", "Pärnu"});
        countriesMap.put("Soome", new String[]{"Helsinki", "Espoo", "Hanko"});
        countriesMap.put("Prantsusmaa", new String[]{"Pariis", "Marseille", "Nizza"});


        //küsi countriesMap-i keyset ja pane see massiivi
        String [] keys= countriesMap.keySet().toArray(String[]::new);
        // võta see massiiv keys ja prindi need riigid ühe kaupa loop-iga välja
        for (int i=0; i<keys.length; i++){// sellega lähen võtmete kaupa sisse
            String myCurrentCountryName = keys[i]; //deklareerim muutuja minu hetkel käsil oleva riigi nime kohta
            System.out.println("Country: " + myCurrentCountryName);


            System.out.println("Cities: ");
            String[] myCurrentCitys =countriesMap.get(myCurrentCountryName);//tegin võtmele vastavast väärtusest massiivi
            // võtmele vastava väärtuse, mis on massiiv saan välja printida for tsükkliga
            for(int j=0; j<myCurrentCitys.length;j++){
                System.out.println("\t"+myCurrentCitys[j]); //  "\t" paneb taande

                //lühem variant for each tsükkliga - proovi ise teha

            }
        }
//System.out.println(estonianEnglishDic.get("maja"));//key-le vastav sõna
        //for(String countryName : countriesMap.keySet());

        //countriesMap.get("Soome"); //kuidas ma saan siit väärtuseks oleva array seest massiivi kätte













    }
}
