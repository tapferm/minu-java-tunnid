package Day4;

import java.util.Scanner;

public class Games {
    public static final int GUESS_COUNT=3;//määran kogu programmis kasutatava muutuja
    public static void main(String[] args) {

        //1.01.1970 - hakkab aja lugemine arvutites
        Scanner scanner = new Scanner(System.in);

        while (true) {
            System.out.println("ALUSTA UUT MÄNGU? Mis vahemikus numbreid tahad liita? ");
            System.out.print("sisesta MINIMAALNE arvuväärtus: ");
            String userInputmin = scanner.nextLine();//nextLine sest nii on lollikindlam nextInt on Oracle teinud bug-e täis
            int minVäärtus = Integer.parseInt(userInputmin);

            System.out.print("sisesta MAKSIMAALNE arvuväärtus: ");
            String userInputmax = scanner.nextLine();//nextLine sest nii on lollikindlam nextInt on Oracle teinud bug-e täis
            int maxVäärtus = Integer.parseInt(userInputmax);

            System.out.println();
            System.out.println("Uus mäng. Arva tehte vastus õigesti " + GUESS_COUNT+" korda");
            long gameStartTime = System.currentTimeMillis() / 1000; //millisekundid on long tüüpi väärtus jaa millisekundites saan sekundid /1000
            boolean hasNotFailed=true;

            for (int i = 0; i < GUESS_COUNT; i++) {
                int tehe=(int) (Math.random()*2);

                int arv1 = (int) (Math.random() *  (maxVäärtus+1-minVäärtus))+minVäärtus;
                int arv2 = (int) (Math.random() *  (maxVäärtus+1-minVäärtus))+minVäärtus;
                int correctAnswer;
                if(tehe==1){
                correctAnswer= arv1+arv2;
                    System.out.print("Ütle tehte vastus: " + arv1 + " + " + arv2 + " = ");
                }else{
                    correctAnswer= arv1-arv2;
                    System.out.print("Ütle tehte vastus: " + arv1 + " - " + arv2 + " = ");
                }
                String userInput = scanner.nextLine();//nextLine sest nii on lollikindlam nextInt on Oracle teinud bug-e täis
                int vastus = Integer.parseInt(userInput);

                    if (vastus == correctAnswer) {
                        System.out.println("Õige vastus. ");

                    } else {
                        System.out.println("MÄNG LÄBI. Vastasid valesti. Õige vastus oleks olnud: " + correctAnswer);
                        hasNotFailed=false;
                        break;//
                        }
            }
            if(hasNotFailed == true){ //täidetakse kui has failed on false
                long gameEndTime = System.currentTimeMillis() / 1000; //millisekundid on long tüüpi väärtus jaa millisekundites saan sekundid /1000
                long duration = gameEndTime - gameStartTime;
                System.out.println("VÕITSID. Vastasid kõik õigest ja sul kulus arvutamiseks " + duration + " sekundit.");
            }
            System.out.println("------------------------------------------------------------");
            Scanner scanner2 = new Scanner(System.in);
            System.out.println("Kas mängid veel ühe mängu? 0=EI / 1=JAH");
            String userInput = scanner2.nextLine();//nextLine sest nii on lollikindlam nextInt on Oracle teinud bug-e täis
            int edasi = Integer.parseInt(userInput);
            if(edasi==0){
                return; //lõpetab töö
            }
        }
    }
}
